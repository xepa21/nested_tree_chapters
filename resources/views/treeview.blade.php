<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="{{url('public/js/tree.js')}}"></script>
<link href="{{url('public/css/tree.css')}}" rel="stylesheet">

<ul id="tree1">

    @if(count($reportsData))
    @foreach($countriesData as $cData)
    @php $cno = 1 @endphp  
    
    <?php
    $reportsData1 = App\CountryReports::where('data->country_id', $cData->id)->where('data->chapter', '=', null)->get();
    ?> 
         
    @if(count($reportsData1))
    <li>ID:{{substr($cData->name,0,3)}}_00{{$cno}}</li>
    <li>Country:{{$cData->name}}</li>
    <li>Language:{{$cData->language}}</li>
    <li><i class="{{@(count($reportsData1) > 0)?'fa fa-plus':'fa fa-minus'}}"></i>Chapters: 
    @php $no = 1 @endphp
    @foreach($reportsData1 as $cParent)
    <?php
    $jsonData = json_decode($cParent->data);
// dd($jsonData->title);
    $cChild = App\CountryReports::where('data->chapter', '=', $cParent->id)->get();
    ?>
    <ul>#{{$no}}:
        <li>ID:{{substr($cData->name,0,3)}}_{{substr($jsonData->title,0,3)}}_00{{$no}}</li>
        <li>Title:{{$jsonData->title}}</li>
        <li>Data:{{$jsonData->data}}</li>
        @if(count($cChild) > 0)
        <li>
            <i class="{{@(count($cChild) > 0)?'fa fa-plus':'fa fa-minus'}}"></i>Sections:               
            @include('manageChild',['childs' => $cChild])
        </li>
        @endif
    </ul>
    @php $no++ @endphp    
    @endforeach
    </li> 
    @endif
    @php $cno++ @endphp 
    @endforeach    
    @endif
</ul>