<ul>
@if(count($childs) > 0)
@php $childno = 1 @endphp
@foreach($childs as $child)
@php
$jsonData = json_decode($child->data);
$cChild = App\CountryReports::where('data->chapter', '=', $child->id)->get();
@endphp
	<ul><i class="{{@(count($cChild) > 0)?'fa fa-plus':'fa fa-minus'}}"></i>#{{$childno}}:
		<li>ID:{{substr($cData->name,0,3)}}_{{substr($jsonData->title,0,3)}}_00{{$childno}}</li>
        <li>Title:{{$jsonData->title}}</li>
        <li>Data:{{$jsonData->data}}</li>
        @if(count($cChild))
        <li>Sections:	
            @include('manageChild',['childs' => $cChild])   
		</li>
		@endif
	</ul>
	@php $childno++ @endphp
@endforeach
@endif
</ul>