<div id="show_errors" style="color: red;"></div> 

{!! Form::open(['method' => 'POST', 'id' => 'submitForm', 'route' => ['add_ajax_data']]) !!}
<div class="form-group">
	<label>Country</label>
	{!! Form::select('country_id', $countries, null, ['class' => 'form-control', 'placeholder' => '']) !!}
	<p class="help-block"></p>
	@if($errors->has('country_id'))
	<p class="help-block">
		{{ $errors->first('country_id') }}
	</p>
	@endif
</div>
<div class="form-group">
	<label>Chapter</label>
	{!! Form::select('chapter', $chapters, null, ['class' => 'form-control', 'placeholder' => '']) !!}
	<p class="help-block"></p>
	@if($errors->has('chapter'))
	<p class="help-block">
		{{ $errors->first('chapter') }}
	</p>
	@endif
</div>
<div class="form-group">
	<label for="title">Title</label>
	{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => '']) !!}
	<p class="help-block"></p>
	@if($errors->has('title'))
	<p class="help-block">
		{{ $errors->first('title') }}
	</p>
	@endif
</div>
<div class="form-group">
	<label>Data</label>
	{!! Form::textarea('data', null, ['class' => 'form-control', 'placeholder' => '']) !!}
	<p class="help-block"></p>
	@if($errors->has('data'))
	<p class="help-block">
		{{ $errors->first('data') }}
	</p>
	@endif
</div>
<div class="form-group">
	{!! Form::button('Submit', ['class' => 'btn btn-info', 'onClick' => 'submitForm()']) !!}    
	{!! Form::close() !!}
</div>
<script type="text/javascript">
	$(document).ready(function() {
		
});
	/*$('form').validate({ // initialize the plugin
	        rules: {
	            title: {
	                required: true
	            },
	            
	        },
	        messages: {
	        	title: {
	        		required:"Please enter title"
	        	},
	        },
	    });*/
</script>