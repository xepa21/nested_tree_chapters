<!DOCTYPE html>
<html>
<head>
	<title>Country Reports</title>
	<!-- Latest compiled and minified CSS -->
	
</head>
<body>
	<div class="col-md-6" id="successMsg" style="color: white;background-color: green;float: right;"></div>
	<div class="col-md-6" id="nested_reports">
		<h3>Country Reports</h3>
		@include('treeview', compact('reportsData'))
		
	</div>
	<div class="col-md-6" id="form_refresh">
		<h3>Add New Chapter/Section</h3>

		@include('country_report_form')
</div>
</body>
@include('head')
<script type="text/javascript">
	$(document).ready(function() {	

		
	});

	function submitForm() {
		// alert("sd");
		var url = "{{url('')}}";
		$.ajax({
		    url : url + "/add_ajax_data",
		    type: "POST",
		    data : $('form').serialize(),
		    success: function(data)
		    {
		    // console.log(data[0].errors.title);
		    var errorArr = [];
		    if(data[0].errors != undefined) {
		    	if(data[0].errors.country_id != undefined) {
		    		var country_id = data[0].errors.country_id[0];
		    	}
		    	if(data[0].errors.title != undefined) {
		    		var title = data[0].errors.title[0];
		    	}
		    	if(data[0].errors.data != undefined) {
		    		var data = data[0].errors.data[0];
		    	}
		    	if(country_id != undefined) {
		    		errorArr.push(country_id);
		    	}
		    	errorArr.push('<br/>');
		    	if(title != undefined) {
		    		errorArr.push(title);
		    	}
		    	errorArr.push('<br/>');
		    	if(data != undefined) {
		    		errorArr.push(data);
		    	}
		    	$("#show_errors").show();
		    	$("#show_errors").empty().html(errorArr);
		    	return false;
		    }
		     $("#nested_reports").empty().html(data);
		     $("#successMsg").text("Chapter added successfully");
		     setTimeout(function() {
		        $("#successMsg").hide();
		    }, 3000);

		     refreshForm();
		    }
		});
	}
	function refreshForm() {
		var url = "{{url('')}}";
		$.ajax({
		    url : url + "/form_refresh",
		    type: "GET",
		    success: function(data)
		    {
		     $("#form_refresh").empty().html(data);
		     
		    }
		});
	};


</script>
</html>