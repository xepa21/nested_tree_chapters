<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CountryReports;

class Country extends Model
{
    protected $table = 'country';
    protected $fillable = [ 'name', 'language'];  

    
}
