<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountryReports extends Model
{
    protected $table = 'country_report';
    protected $fillable = ['data'];

    
}
