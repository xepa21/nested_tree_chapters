<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\CountryReports;
use Validator;

class CountryController extends Controller
{
    public function __construct()
    {
        
    }

    public function index() {
    	$country_report = CountryReports::get();
    	$countries = Country::pluck('name', 'id')->prepend('Select', '');
    	$chapter = CountryReports::where('data->chapter', '!=', '')->get();
    	$chapters = [];
    	foreach ($chapter as $key => $value) {
    		$decodedVal = json_decode($value->data);
    		// dd($decodedVal->chapter);
    		$chapters[$value->id] = $decodedVal->title; 
    	}

    	//Tree view data
    	$countriesData = Country::get();
    	$reportsData = CountryReports::where('data->chapter', '=', null)->get();
    	// dd($countriesData);
    	return view('country_report', compact('country_report', 'countries', 'chapters', 'reportsData', 'countriesData'));
    }

    public function add_ajax_data(Request $request) {

    	$validator = Validator::make($request->all(), [
    		'country_id' => 'required',
	        'title' => 'required',
	        'data' => 'required'
	    ]);
	    if ($validator->fails()) {    
	    	$errors[] = array('errors' => $validator->errors());
		    return response()->json($errors, 200);
		}
    	// dd($request->chapter);
    	$finaldata = array('country_id' => $request->country_id, 'chapter' => @$request->chapter,'title' => $request->title, 'data' => $request->data);

    	$finalJson = json_encode($finaldata);

    	$countryReport = CountryReports::create(['data' => $finalJson]);

    	$reportsData = CountryReports::where('data->chapter', '=', null)->get();
    	$countriesData = Country::get();
    	return view('treeview', compact('reportsData', 'countriesData'))->render();
    }

    public function form_refresh() {
    	$countries = Country::pluck('name', 'id')->prepend('Select', '');
    	$chapter = CountryReports::where('data->chapter', '!=', '')->get();
    	$chapters = [];
    	foreach ($chapter as $key => $value) {
    		$decodedVal = json_decode($value->data);
    		// dd($decodedVal->chapter);
    		$chapters[$value->id] = $decodedVal->title; 
    	}
    	return view('country_report_form',compact('countries', 'chapters'))->render();
    }
}
