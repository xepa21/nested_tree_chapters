<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
	    App\Country::create([
            'name' => 'India',
            'language' => 'English'
        ]);

        App\Country::create([
            'name' => 'USA',
            'language' => 'English'
        ]);

        App\Country::create([
            'name' => 'Netherland',
            'language' => 'Dutch'
        ]);
    }
}
