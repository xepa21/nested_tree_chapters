<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CountryController@index');
Route::post('/add_ajax_data', 'CountryController@add_ajax_data')->name('add_ajax_data');
Route::get('/form_refresh', 'CountryController@form_refresh');
